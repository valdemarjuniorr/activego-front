#!/bin/bash
bucket=s3://joinpersonal.com.br

echo "Updating production"
aws s3 rm ${bucket}/static/
aws s3 rm ${bucket}/index.html

echo "Updating AWS S3 with new version files"
aws s3 sync dist/ ${bucket}

echo "Update Done!"
