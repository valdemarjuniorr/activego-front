import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

// Alunos
import AlunoCadastrar from '@/views/alunos/Cadastrar'
import AlunoListar from '@/views/alunos/Listar'
import AlunoListarInativos from '@/views/alunos/ListarInativos'
import AlunoDetalhar from '@/views/alunos/Detalhar'

// Avaliações
import CompararAvaliacoes from '@/views/avaliacao/CompararAvaliacoes.vue'
import AvaliacoesCadastrar from '@/views/avaliacao/Cadastrar.vue'

// Perimetria
import PerimetriaCadastrar from '@/views/perimetria/Cadastrar'
import PerimetriaListar from '@/views/perimetria/Listar'

// Adipometria
import AdipometriaCadastrar from '@/views/adipometria/Cadastrar'

// Anamnese
import AnamneseCadastrar from '@/views/anamnese/Cadastrar'
import AnamneseDetalhar from '@/views/anamnese/Detalhar'
import AnamneseOuAvaliacao from '@/views/anamnese/AnamneseOuAvaliacao'

// components
import Full from '@/container/Full'
// dashboard views
import DashboardOne from '@/views/DashboardOne'

// Perfil personal
import PersonalDetalhar from '@/views/personal/Perfil'
import PersonalEditar from '@/views/personal/Editar'

// session views
import Login from '@/views/session/Login'
import Logout from '@/views/session/Logout'
import SignUp from '@/views/session/SignUp'

// define your routes here
var router = new Router({
  routes: [
    {
      path: '/dash',
      component: Full,
      children: [
        {
          path: '',
          component: DashboardOne,
          meta: {
            breadcrumb: 'Visão geral',
            requiresAuth: true
          }
        },
        {
            path: '/personal/perfil',
            component: PersonalDetalhar,
            meta: {
              breadcrumb: 'Perfil',
              requiresAuth: true
            }
        },
        {
            path: '/personal/editar',
            component: PersonalEditar,
            meta: {
              breadcrumb: 'Editar perfil',
              requiresAuth: true
            }
        },
        {
          path: '/alunos/Listar',
          component: AlunoListar,
          meta: {
            breadcrumb: 'Lista de alunos',
            requiresAuth: true
          }
        },
        {
          path: '/alunos/ListarInativos',
          component: AlunoListarInativos,
          meta: {
            breadcrumb: 'Lista de ex-alunos',
            requiresAuth: true
          }
        },
        {
          path: '/alunos/Detalhar',
          component: AlunoDetalhar,
          meta: {
            breadcrumb: 'Dados aluno',
            requiresAuth: false
          }
        },
        {
          path: '/avaliacao/comparar',
          component: CompararAvaliacoes,
          meta: {
            breadcrumb: 'Compara avaliações',
            requiresAuth: false
          }
        },
        {
            path: '/avaliacao/Cadastrar',
            component: AvaliacoesCadastrar,
            meta: {
              breadcrumb: 'Avaliações',
              requiresAuth: true
            }
          },
        {
          path: '/alunos/Cadastrar',
          component: AlunoCadastrar,
          meta: {
            breadcrumb: 'Novo aluno',
            requiresAuth: true
          }
        },
        {
          path: '/perimetria/Listar',
          component: PerimetriaListar,
          meta: {
            breadcrumb: 'Lista de alunos'
          }
        },
        {
          path: '/perimetria/Cadastrar',
          component: PerimetriaCadastrar,
          meta: {
            breadcrumb: 'Dados Perimetria ',
            requiresAuth: true
          }
        },
        {
          path: '/adipometria/Cadastrar',
          component: AdipometriaCadastrar,
          meta: {
            breadcrumb: 'Dados Adipometria ',
            requiresAuth: true
          }
        },
        {
          path: '/anamnese',
          component: AnamneseCadastrar,
          meta: {
            breadcrumb: 'Anamnese ',
            requiresAuth: false
          }
        },
        {
          path: '/anamnese/detalhar',
          component: AnamneseDetalhar,
          meta: {
            breadcrumb: 'Dados anamnese ',
            requiresAuth: false
          }
        },
        {
          path: '/anamnese-avaliacao',
          component: AnamneseOuAvaliacao,
          meta: {
            breadcrumb: 'Pergunta',
            requiresAuth: false
          }
        }
      ]
    },
    {
      path: '/',
      name: 'Login',
      component: Login,
      meta: {
        requiresAuth: false
      }
    },
    {
      path: '/logout',
      name: 'logout',
      component: Logout,
      meta: {
        requiresAuth: false
      }
    },
    {
      path: '/login',
      component: Login,
      meta: {
        requiresAuth: false
      }
    },
    {
      path: '/sign-up',
      name: 'Sign Up',
      component: SignUp
    }
  ]
})
var cookies = require('js-cookie');
router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
  if (requiresAuth) {
    var usuarioLogado = cookies.getJSON('personalLogado')
    if (usuarioLogado === undefined) {
      next('Login');
      return
    }
  }
  next();
})
export default router
