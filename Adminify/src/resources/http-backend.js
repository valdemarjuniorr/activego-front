import * as http from './http';

const BACKEND_URL = `${process.env.BACKEND_URL}:8089`

export function get(url, params = {}) {
  return http.get(BACKEND_URL + url, params)
}

export function post(url, data) {
  return http.post(BACKEND_URL + url, data)
}

export function put(url, data) {
  return http.put(BACKEND_URL + url, data)
}

export function remove(url, params = {}) {
  return http.remove(BACKEND_URL + url, params)
}
