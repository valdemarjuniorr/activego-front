import axios from 'axios';

export function get(url, params = {}) {
  const config = {
    url,
    params,
    method: 'GET'
  };

  return request(config);
}

export function post(url, data) {
  const config = {
    url,
    data,
    method: 'POST'
  };

  return request(config);
}

export function put(url, data) {
  const config = {
    url,
    data,
    method: 'PUT'
  };

  return request(config);
}

export function remove(url, params = {}) {
  const config = {
    url,
    params,
    method: 'DELETE'
  };

  return request(config);
}

function request(config) {
  // config.withCredentials = true;
  config.headers = {
    accept: 'application/json'
  };
  return axios.request(config);
}
