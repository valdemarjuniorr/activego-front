// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'babel-polyfill'
import Vue from 'vue'
import VeeValidate from 'vee-validate';
import Nprogress from 'nprogress'
import App from './App'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import wysiwyg from 'vue-wysiwyg'
import VueBreadcrumbs from 'vue2-breadcrumbs'

// adminify scripts
import './lib/adminifyScripts'

// include all css files
import './lib/adminifycss'

// navigation guards before each
router.beforeEach((to, from, next) => {
  Nprogress.start()
  next()
})

// navigation guard after each
router.afterEach((to, from) => {
  Nprogress.done()
})

Vue.use(BootstrapVue)
Vue.use(wysiwyg,
  {
    hideModules: {
      "underline": true,
      "headings": true,
      "code": true,
      "image": true,
      "table": true,
      "removeFormat": true
    },
    forcePlainTextOnPaste: true
  }
)
Vue.use(VueBreadcrumbs)

Vue.config.productionTip = false

Vue.use(VeeValidate, {
  aria: true,
  classNames: {},
  classes: false,
  delay: 0,
  dictionary: null,
  errorBagName: 'errors', // change if property conflicts
  events: 'input|blur',
  fieldsBagName: 'fields',
  i18n: null, // the vue-i18n plugin instance
  i18nRootKey: 'validations', // the nested key under which the validation messages will be located
  inject: true,
  locale: 'en',
  validity: false
});

const dictionary = {
  en: {
    messages: {
      required: (field) => `O campo ${field} é obrigatório.`,
      min: (field, value) => `O campo ${field} tem que ter pelo menos ${value} caracteres.`,
      email: (field) => `O campo ${field} está inválido.`,
      min_value: (field) => `O campo ${field} é obrigatório.`
    }
  }
};

// Register a global custom directive called v-focus
Vue.directive('focus', {
  // When the bound element is inserted into the DOM...
  inserted: function (el) {
    // Focus the element
    el.focus()
  }
});

VeeValidate.Validator.localize(dictionary)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
