import {Pie} from 'vue-chartjs'

export default {
  props: ['Masculino', 'Feminino'],
  extends: Pie,
  data() {
    return {
      options: {
        legend: {
          position: 'bottom',
          labels: {
            fontSize: 12,
            usePointStyle: true
          }
        }
      }
    }
  },
  methods: {
    init() {
      this.renderChart({
        labels: [
          `Feminino`,
          `Masculino`
        ],
        datasets: [{
          data: [this.Feminino, this.Masculino],
          backgroundColor: [
            '#ff005f',
            '#00aeec'
          ],
          borderWidth: [1, 1],
          hoverBackgroundColor: [
            '#e00053',
            '#009dd5'
          ]
        }]
      }, this.options)
    }
  },
  mounted() {
    this.init()
  }
}
