import { Bar } from 'vue-chartjs'

export default {
  props: ['avaliacoes'],
  extends: Bar,
  data() {
    return {
      options: {
      }
    }
  },
  methods: {
    arredondar(valor) {
        var numeral = require('numeral');
        return numeral(valor).format('0.0[0]')
    },
    init() {
        var index = 0
        var percGordura = []
        var massaMagra = []
        var massaGorda = []
        this.avaliacoes.forEach(av => {
            percGordura[index] = this.arredondar(av.adipometria.percentualgordura)
            massaMagra[index] = this.arredondar(av.adipometria.massamagra)
            massaGorda[index] = this.arredondar(av.adipometria.massagorda)
            index += 1
        });
        this.renderChart({
            labels: ['Avaliação atual', 'Avaliação anterior'],
            datasets: [
              {
                label: '% gordura',
                backgroundColor: '#17C7DF',
                data: percGordura
              },
              {
                label: 'Massa magra',
                backgroundColor: '#F64A32',
                data: massaMagra
              },
              {
                label: 'Massa gorda',
                backgroundColor: '#FEA200',
                data: massaGorda
              }
            ],
            borderWidth: [0, 0, 0]
        }, this.options)
    }
  },
  mounted() {
    this.init()
  }
}
