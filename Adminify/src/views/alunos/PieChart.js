import { Pie } from 'vue-chartjs'

export default {
  props: ['percentualgordura', 'massamagra', 'massagorda'],
  extends: Pie,
  data() {
    return {
      options: {
        legend: {
          position: 'bottom',
          labels: {
            fontSize: 12,
            usePointStyle: true
          }
        }
      }
    }
  },
  methods: {
    arredondar(valor) {
        var numeral = require('numeral');
        return numeral(valor).format('0.0[0]')
    },
    init() {
        var percGordura = this.arredondar(this.percentualgordura)
        var massaMagra = this.arredondar(this.massamagra)
        var massaGorda = this.arredondar(this.massagorda)
        this.renderChart({
            labels: [
                '% gordura( ' + percGordura + ' % )',
                'Massa magra( ' + massaMagra + ' % )',
                'Massa gorda( ' + massaGorda + ' kg )'
            ],
            datasets: [{
              data: [percGordura, massaMagra, massaGorda],
              backgroundColor: [
                '#17C7DF',
                '#F64A32',
                '#FEA200'
              ],
              borderWidth: [1, 1, 1],
              hoverBackgroundColor: [
                '#17C7DF',
                '#F64A32',
                '#FEA200'
              ]
            }]
        }, this.options)
    }
  },
  mounted() {
    this.init()
  }
}
