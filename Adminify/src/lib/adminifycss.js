import '../assets/themify-icons/themify-icons.css'
import 'vue-wysiwyg/dist/vueWysiwyg.css'
import 'font-awesome/css/font-awesome.min.css'
import 'vue2-dragula/styles/dragula.css'

// nprogress
import 'nprogress/nprogress.css'

// Animation css
import '../assets/animate/font-awesome-animation.css'

/* bootstrap */
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import '../assets/scss/style.scss'
